#### gish
**Version control system for custom development**  
Object types - text files and directories only  
  * Install:  
Locate the script `gish` in the directory `/usr/local/bin/`  
```none
gish
mkdir project
cd project
gish init
```
  * Next:  
```none
gish intro
gish help
```
